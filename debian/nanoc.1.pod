=encoding UTF-8

=head1 NAME

nanoc - Nanoc, a static site compiler written in Ruby

=head1 SYNOPSIS

B<nanoc> I<command> [I<options>] [I<arguments>]

=head1 DESCRIPTION

B<Nanoc> is a static site generator, fit for building anything from a small
personal blog to a large corporate web site. It can transform content
from one format (e.g. Haml or Markdown) into another (usually HTML) and
lay out pages so that the site's look and feel is consistent across all
pages.



=head1 COMMANDS

=over

=item B<check>

run issue checks

=item B<compile>

compile items of this site

=item B<create-site>

create a site

=item B<deploy>

deploy the compiled site

=item B<help>

show help

=item B<prune>

remove files not managed by Nanoc from the output directory

=item B<shell>

open a shell on the Nanoc environment

=item B<show-data>

show data in this site

=item B<show-plugins>

show all available plugins

=item B<show-rules>

describe the rules for each item

=item B<view>

start the web server that serves static files

=back

=head1 OPTIONS

=over

=item B<-C>, B<--no-color>

disable color

=item B<-V>, B<--verbose>

make nanoc output more detailed

=item B<-d>, B<--debug>

enable debugging

=item <-e>, B<--env>=I<value>

set environment

=item B<-h>, B<--help>

show the help message and quit

=item B<-l>, B<--color>

enable color

=item B<-v>, B<--version>

show version information and quit

=item B<-w>, B<--warn>

enable warnings

=back

=head1 AUTHORS

B<Nanoc> has been developed by Denis Defreyne <denis.defreyne@stoneship.org>.
