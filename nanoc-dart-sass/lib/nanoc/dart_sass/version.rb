# frozen_string_literal: true

module Nanoc
  module DartSass
    VERSION = '1.0.1'
  end
end
